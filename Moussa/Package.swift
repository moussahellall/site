// swift-tools-version:5.2

import PackageDescription

let package = Package(
    name: "Moussa",
    products: [
        .executable(
            name: "Moussa",
            targets: ["Moussa"]
        )
    ],
    dependencies: [
        .package(name: "Publish", url: "https://github.com/johnsundell/publish.git", from: "0.6.0")
    ],
    targets: [
        .target(
            name: "Moussa",
            dependencies: ["Publish"]
        )
    ]
)