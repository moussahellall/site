---
date: 2020-08-29 17:00
description: Sets in Swift Simplified overly simplified.
tags: Sets, Swift, Swift 5, iOS
---

# Sets in Swift Simplified

**Sets** are there to store **different unordered values**. Unlike arrays that can store a bunch of duplicate and ordered ones.

Sets only allow storing types that conform to **Hashable** protocol such as the basic Swift types.

The point is if you are aiming to store a **collection** of **distinct values** of type **hashable** not caring about its order the answer is a set. Thus, let’s stop babbling around, and get our hands dirty with some code.

creating a Set of Type Int :

<script src="https://gist.github.com/MoussaHellal/be9a3158ea126d40ecd80edb22569032.js"></script>

let’s insert some value:

<script src="https://gist.github.com/MoussaHellal/bb0d1861313ba08a10c849ccfde3686f.js"></script>

add more values to try it yourself.

count our set by using:

<script src="https://gist.github.com/MoussaHellal/a9687e26828e9b6fd401bb62ba4d82df.js"></script>

Now, removing an element or removing all elements by using these simple two lines:

<script src="https://gist.github.com/MoussaHellal/2a50ed43b523f5e6e7136544e016230d.js"></script>

For now, let’s see if our set is empty:

<script src="https://gist.github.com/MoussaHellal/92fda610fb78e28c8283b27f1c50c575.js"></script>

and check if the set has a specific value:

<script src="https://gist.github.com/MoussaHellal/c1b24ed38c8881d8e8e9f71d75742829.js"></script>

As you saw it is very simple to interact and play around with a set.

What about basic set operations such as Union, subtracting, and intersection?
Here is your answer:

<script src="https://gist.github.com/MoussaHellal/009e36e1e4e1393988a12a9f39982663.js"></script>

That’s it for this article. I hope you have learned something new today. See you in the next one.
