---
date: 2020-08-29 17:00
description: Explaining the pattern with a case study.
tags: Notification, Observer, iOS, programming, 
---

# iOS: Implementing the Notification/Observer Communication Pattern

### Explaining the pattern with a case study

![Photo by [Dennis Brendel](https://unsplash.com/@dnnsbrndl?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/search/photos/ios?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)](https://cdn-images-1.medium.com/max/800/1*Zb6W50gQNY1po5hyK-byAQ.jpeg)

First, let’s explain what the Notification/Observer communication pattern is and how it works. The latter is a very simple and straightforward concept to understand.

The Observer: The recipient which is waiting for the notification to be triggered, to execute whatever code you want it to.

The Notification: The actual notification that it is going to be fired whenever we intend the Observer to perform a certain action. And, we can have as many notifications as we want, all over the app.

So, by this, we understand that the Observer/Notification is a communication pattern, based on many to one. By *many*, we meant the Notification(s) and by *one* we mean the Observer.

Easy? It is actually.

A case study will help us understand:

Let’s assume that we have a shop application with a cart view that has a notification badge, and other views where we can shop for the intended product. Whenever the user shops for a new item, it will be added to his cart whilst a badge is updated.

Let’s jump right into the code.

First of all, please go ahead and [download the project](https://github.com/MoussaHellal/NotificationObserverSample), so we waste no time on starting a new project and building a few boilerplate views. Hence, we can focus only on what matters in this article.

For simplicity’s sake, this project:

Is a tabbed app.

Has three very simple views.

Simple views:

My cart view: Nothing, only the badge indicating how many products are shopped for.

Two other views: food and soda.

Launch the project from the before folder. Otherwise, you can just start the project from the after folder and follow along with me with what we are going to do here.

As a first step, we will add our Notification.Name object globally in the GlobalVariables file, so we can access it whenever we want to:

<script src="https://gist.github.com/MoussaHellal/dd80a3d2386b7dce0fed212abda7b9e0.js"></script>

Now, we are going to create our Observer and the function we are going to execute whenever a notification is triggered in the TabBarController:

 <script src="https://gist.github.com/MoussaHellal/aa700319ef1dbf5d015b8614bb409d33.js"></script>

Then, let’s add the createObservers() function to viewDidLoad() in the same file TabBarController.

<script src="https://gist.github.com/MoussaHellal/2b55765ebeb091ff11cecafcb878c639.js"></script>

We’ve added our createObservers() function to the viewDidLoad() so whenever the TabBarController is allocated into memory, the Observer is going to be created.

At the moment, our TabBarController file must look like this:

 <script src="https://gist.github.com/MoussaHellal/e01e08ea34fd31fe8cacc5b07d33d2f7.js"></script>

Now is the time for our Notification placement on both view controllers Food and Soda:

Open up the SodaController file. Add this additional line to the button action addImaginarySoda().

 <script src="https://gist.github.com/MoussaHellal/7c964fd10f3af8e39a0dbd155de0ed67.js"></script>

Do the same for FoodController, but for addImaginaryFood().

Finally, run the app and click the *add* button on *Food* or *Soda* View:

![](https://cdn-images-1.medium.com/max/800/1*kjFly4sJBno8Uy-4GR9Q8A.png)

Notice that, after you clicked the *add* button, the badge on top of the *My Cart* icon is updated through the setBadge() function.

An Update: 
Please do not forget to add this to your ViewController that holds the Observers to deallocate it from the memory:

<script src="https://gist.github.com/MoussaHellal/1a73145494ebbd7e338c14a730263721.js"></script>

That’s it, thank you for reading my very first article on Medium.
