
---
date: 2020-08-29 17:00
description: Protocol/Delegate in iOS.
tags: Protocol, Delegate, iOS Protocol Delegate, Communication Pattern
---
# iOS: Implementing the Protocol/Delegate Communication Pattern

![The app displayed here is actually what we are going to build.](https://cdn-images-1.medium.com/max/800/1*CeMtsYQDaAZrD89fTZWo-Q.png)

Hello, I hope you all are doing great and ready for this brief tutorial.

As a start, The Protocol/Delegate in iOS is specified as one to one communication pattern. In other words, it is the transmission of some information from an object to another object.

As usual, let’s take a real-world example of the subject. Imagine yourself as the CEO of some great space travel agency and one of the employees comes to you asking if the launch of the spaceship should be afternoon or tomorrow. And for the safety fo the crew, you order that it’ll be tomorrow.

So here, as you can see the employee comes to you asking you for a decision and you gave him back that decision. The transmission of information from the employee to you, then you to the employee is simply what this communication pattern can do between objects.

Aghh you might be already bored! So let’s jump into some code and do some real illustration for this widely used pattern in iOS. By the way, it is very easy to understand and implement.

You can download the project from [here in Github](https://github.com/MoussaHellal/ProtocolDelegateSample). If you would like to implement with me the sample project for the matter you can just start with the before project and write some code with me, otherwise, you can just launch that after project and follow along what we are going to implement.

For simplicity’s sake, this project is:

* A simple app that displays a chosen moon or a planet image, and it is divided into two views.

* One that contains the main displayed image

* The other holds a set of choices on which we’d like to see in the main view.

This is the app in images

![](https://cdn-images-1.medium.com/max/800/1*gizlvBJRo7iJKBMvsP-URw.jpeg)

And in code in the MainViewController:

 <script src="https://gist.github.com/MoussaHellal/0752eb9060ebae94c6b8d73a92410532.js"></script>

So here in changeButtonDidTapped we are just presenting the ChooseViewController in which its code got:

<script src="https://gist.github.com/MoussaHellal/beaef1afc1bd5cab053bbe8462aa294d.js"></script>

!! Reminder: you can find all the code provided in this article [here in Github](https://github.com/MoussaHellal/ProtocolDelegateSample).

the ChooseViewController only got those buttons actions when clicked the current view got dismissed and we get back to the MainViewController.

Anyway, in this situation we need that protocol/delegate so whenever that ChooseViewController is opened we can choose which planet or moon we’d like to display in Main View.

As we said earlier it’s an ‘object to object’ communication and in our example Here:

* The MainViewController is asking the ChooseViewController which space object it should display.

* The ChooseViewController responds back with the image to display.

* The MainViewController then displays what image did came from the ChooseViewController.

Let’s add a new file to our project and add this protocol into it :

<script src="https://gist.github.com/MoussaHellal/03159305287603a7b27cb5a6c6fc1dec.js"></script>

Now you have added the protocol let’s conform it in our MainViewController so we can use it later on:

<script src="https://gist.github.com/MoussaHellal/3e53b5e66f55d4aafb86a6563b28afa8.js"></script>

As you can see we implemented didChoosePlanetWith(image: UIImage)method, thus when our delegate has been hit, in other words, when our method is called from ChooseViewController through the delegate, we are just changing the main image in the view to the passed one.

Add this line of code to ChooseViewController file so we can pass object and use the delegation with our MainViewController :

 <script src="https://gist.github.com/MoussaHellal/f815073ad75cc3ced9334354f8c3762b.js"></script>

Get back to MainViewController and pass our delegate to ChooseViewController through adding delegate to self as illustrated below in code :

 <script src="https://gist.github.com/MoussaHellal/c79edc7694aef0e59b0d5e521f95e337.js"></script>

Now, only one thing last which is when clicking on any planet/moon button we should pass its image though calling delegate.didChoosePlanetWith(image:UIImage(named: "image-name")!) ****in every button clicked as you can see how our ChooseViewController became:

 <script src="https://gist.github.com/MoussaHellal/0ade33457b9331a218f4ca034fb17e60.js"></script>

We are passing every planet/moon image through our protocol required method and force unwrapping it because we already know that these images are in the assets of the project.

The end result:

![](https://cdn-images-1.medium.com/max/800/1*BbMzXujFa8BDn_IACS6WYg.gif)

We came to the end of this article. First, thank you for your time , and if you find this useful in any way or wanna get in touch with me.

You can find me on Twitter
[**Moussa Hellal**
*The latest Tweets from Moussa Hellal (@HellalMoussa). Passionate iOS Developer. *twitter.com](https://twitter.com/HellalMoussa)
