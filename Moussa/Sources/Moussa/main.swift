import Foundation
import Publish
import Plot

// This type acts as the configuration for your website.
struct Moussa: Website {
    enum SectionID: String, WebsiteSectionID {
        // Add the sections that you want your website to contain here:
        case articles
        case portfolio
        case about
    }
    
    struct ItemMetadata: WebsiteItemMetadata {
        // Add any site-specific metadata that you want to use here.
    }
    
    // Update these properties to configure your website:
    var url = URL(string: "https://moussadev.com")!
    var name = "Moussa Hellal"
    var description = "About"
    var language: Language { .english }
    var imagePath: Path? { nil }
}

private extension Node where Context == HTML.BodyContext {
    static func wrapper(_ nodes: Node...) -> Node {
        .div(.class("wrapper"), .group(nodes))
    }
    
    static func itemList<T: Website>(for items: [Item<T>], on site: T) -> Node {
        return .ul(
            .class("item-list"),
            .forEach(items) { item in
                .li(.article(
                    .h1(.a(
                        .href(item.path),
                        .text(item.title)
                        )),
                    .p(.text(item.description))
                    ))//li
            }//foreach
        )//ul
    }
    
    static func myHeader<T: Website>(for context: PublishingContext<T>) -> Node {
        header(
            .wrapper(
                .img(.class("profileImage"),.src("https://moussadev.com/images/moussa.png")),
                .nav(
                    .a(
                        .href(context.site.url),
                        .class("site-name"),
                        .text(context.site.name))
                ),
                //.div(.class("divCentered"),
                .ul(
                    .class("navSections"),
                    .li(.a(.href(context.site.url),.text("Home"))),
                    .forEach(context.sections, { (item)  in
                        
                        .li(
                            .a(
                                .href(item.path),
                                .text(item.title)
                            )//a
                        )//li
                    }
                    )//foreach
                )//ul
                //)//div
            )//nav
        )//wrapper
    }
    
    static func myFooter<T: Website>(for context: PublishingContext<T>) -> Node {
        footer(
            .a(
                .text("Hello world")
            )
        )//wrapper
    }
}

struct MyHtmlFactory<Site: Website>: HTMLFactory {
    func makeIndexHTML(for index: Index, context: PublishingContext<Site>) throws -> HTML {
        HTML(
            .head(for: index, on: context.site),
            .body(
                .myHeader(for: context),
                .wrapper(
                    //.p(.text("&ensp;")), // for space
                    //MARK: - About Me
                    .h2(.class("inlineTitle"), .text("About Me")),
                    .p(.text("")),// for spaces
                    .p(.text("")),// for spaces
                    .p(.text("")),// for spaces
                    .div(.class("socialLinks"),
                         .a(.href("https://twitter.com/HellalMoussa"), .img(.class("scoialIcons"), .src("images/twitter.svg"))),
                         .a(.href("https://medium.com/@moussahellal"), .img(.class("scoialIcons"), .src("images/medium.svg"))),
                         .a(.href("https://www.linkedin.com/in/moussa-hellal-834418167"), .img(.class("scoialIcons"), .src("images/linkidin.svg")))
                    ),//Div social icons links
                    
                        .p(.class("aboutMeText"), .text("I'm Moussa, a software developer with 5+ years of experience. I started my career as a Backend developer and have spent the past 4 years focused on iOS Development." )),
                    
                    //MARK: LONG INTRODUCTION
//                    .p(.class("aboutMeText"), .text("Hello, my name is Moussa, and I have over 5 years of experience as a software developer. I began my career as a Backend developer, where I gained valuable expertise in building scalable and efficient systems." )),
//                    .p(.class("aboutMeText"), .text("After a year or so in Backend development, I discovered my passion for mobile development and decided to transition to iOS Development. Over the past 4 years, I have honed my skills in developing iOS applications and focused on creating high-quality, user-friendly, and engaging applications that meet the ever-changing needs of mobile users." )),
                    .p(.text("&ensp;")), // for space
                    //MARK: - Recent Post
                    .h2(.class("inlineTitle"), .text("Recent Posts 📄")),
                    .a(.href(URL(string: "#")!), .class("rightElementSeeAll inlineTitle"), .text("See More")),
                    //.p(.text("&ensp;")), // for space
                    .ul(
                        .class("item-list"),
                        .forEach(context.allItems(sortedBy: \.date, order: .descending), { item in
                            .li(
                                .article(
                                    .h1(
                                        .a(
                                            .href(item.path),
                                            .text(item.title)
                                        )//a
                                    ), //h1
                                    .p(.text(item.description))
                                )
                            )
                        })
                    ),
                    .p(.text("&ensp;")), // for space
                    //MARK: - first TADAWI PORTFOLIO PROJECT
                    .h2(.class("inlineTitle"), .text("Portfolio 📱")),
                    
                    //MARK: - CW PORTFOLIO PROJECT
                    .p(.text("&ensp;")), // for space
                    .h2(.class("inlineTitle"), .text("‎‎‏‏‎ ‎")),
                    .h1(.class("portfolioAppTitle"), .text("  Care.Labs Wallet")),
                    .p(.text("")),
                    .img(.class("portfolioAppIcon"),.src("images/care.labs_logo.png")),
                    .p(.text("")),
                    
                        .div(.class(" "),
                             .p(.class("projectDescription"),.text(" Mainly for businesses that want to build and leverage their solutions on top of Blockchain technology.")),
                             .p(.class("projectDescription"),.text("Care.Labs Wallet is a breakthrough in Web 3.0 development. It is the app for digital decentralized health network (Care.Network) authors. Using this app, it can easily create a complete commercial and market ready applications also known as Care.Networks.")),
                            
                             .p(.class("projectDescription"),.text("My duty was mainly around what's next, provide solutions suggestion to architects, communicating with products managers, designers, as well as be part of cross team collaboration, and lead the iOS dev team while also being involved in building the apps."))
                        ),
                    
                        .div(.class("click-zoom-4-screens"),
                             //.label(
                            //      .input(.type(.checkbox)),
                            .img(.src("images/carelabswalletScreenshots.png"))
                            //)//label
                        ),

                        .img(.class("technologiesUsed"),.src("images/cwlcwltech.png")),
                                            
                        .div(.class("installAppStore"),
                             .a(.href("https://apps.apple.com/us/app/care-labs-wallet/id1621537897"),.img(.class("installAppStore"),.src("images/downloadOnAppStore.png")))
                        ),
                   
                    .p(.text("")),
                    .img(.class("portfolioAppIcon"),.src("images/care.wallet_logo.png")),
                    .p(.text("")),
                    .h1(.class("portfolioAppTitle"), .text("  Care.Wallet")),
                    .a(.href(URL(string: "#")!), .class("rightElementSeeAll inlineTitle"), .text("See More")),
                    .p(.text("")),
                                       
                    .div(.class(" "),
                         .p(.class("projectDescription"),.text("Mainly for consumers who want to register their history records, interact with other members within the blockchain, and collect their ECG data through devices such as AliveCor.")),
                         
                         .p(.class("projectDescription"),.text("Care.Wallet uses blockchain technology and puts the user in complete control of who has access to their data, top-up wallet SOLVE balance. Furthermore, a huge part of the app is based on declared and dynamic views into iOS as Server-driven-UI.")),
                        
                         .p(.class("projectDescription"),.text("My duty was mainly around what's next, provide solutions suggestion to architects, communicating with products managers, designers, as well as be part of cross team collaboration, and lead the iOS dev team while also being involved in building the apps."))
        
                        
                    ),//div
                    .div(.class("click-zoom-4-screens"),
                         //.label(
                        //      .input(.type(.checkbox)),
                        .img(.src("images/cwScreenshots.png"))
                        //)//label
                    ),
                    
                        .div(.class(" "),
                        .img(.class("technologiesUsed"),.src("images/cwlcwltech.png")),
                        
                    
                        .div(.class("installAppStore"),
                             .a(.href("https://apps.apple.com/us/app/care-wallet/id1434240426"),.img(.class("installAppStore"),.src("images/downloadOnAppStore.png")))
                        )
                    ),
                    
                    .p(.text("&ensp;")), // for space
                    
                    
                    
                    
                    //MARK: - Second Swapfiets PORTFOLIO PROJECT
                    .p(.text("&ensp;")), // for space
                    .h2(.class("inlineTitle"), .text("‎‎‏‏‎ ‎")),
                    .p(.text("")),
                    .img(.class("portfolioAppIcon"),.src("images/swapfiets_logo.png")),
                    .p(.text("")),
                    .h1(.class("portfolioAppTitle"), .text("  Swapfiets")),
                    .a(.href(URL(string: "#")!), .class("rightElementSeeAll inlineTitle"), .text("See More")),
                    .p(.text("")),
                                       
                    .div(.class(" "),
                         .p(.class("projectDescription"),.text("Worked with a Dutch agency LabelA as a Contractor for Swapfiets the app to help deliver a new version (3.3.0) and fix few bugs.")),
                         .p(.class("projectDescription"),.text("My main mission was to add new features to the iOS application; such as collecting user feedback within the app with Usabilla Framework. As well as updates to UI and bug fixes."))
        
                        
                    ),//div
                    .div(.class("click-zoom-4-screens"),
                         //.label(
                        //      .input(.type(.checkbox)),
                        .img(.src("images/swapfiets4screens.png"))
                        //)//label
                    ),
                    
                        .div(.class(" "),
                        .img(.class("technologiesUsed"),.src("images/cwlcwltech.png")),
                        
                    
                        .div(.class("installAppStore"),
                             .a(.href("https://apps.apple.com/us/app/swapfiets/id1330923084"),.img(.class("installAppStore"),.src("images/downloadOnAppStore.png")))
                        )
                    ),
                    
                    .p(.text("&ensp;")), // for space
                    
                    
                    //MARK: - Second SPARE PORTFOLIO PROJECT
                    .p(.text("&ensp;")), // for space
                    .h2(.class("inlineTitle"), .text("‎‎‏‏‎ ‎")),
                    .p(.text("")),
                    .img(.class("portfolioAppIcon"),.src("images/SpareCropped.png")),
                    .p(.text("")),
                    .h1(.class("portfolioAppTitle"), .text("  SPARE")),
                    .a(.href(URL(string: "#")!), .class("rightElementSeeAll inlineTitle"), .text("See More")),
                    .p(.text("")),
                    
                    //.h2(.class("inlineTitle"), .text("Tadawi")),
                    .div(.class("click-zoom"),
                         //.label(
                        //      .input(.type(.checkbox)),
                        .img(.src("images/spareOne.png"))
                        //)//label
                    ),
                    .div(.class("divPortfolio"),
                         .p(.class("projectDescription"),.text(" A fin-tech app that allows users to track and analyze spendings. Set budget and so on and so forth.")),
                         .p(.class("projectDescription"),.text(" Being in a team while building Spare app with multiple technologies like:")),
                         .p(.b("• Realm:"),.text("a persistent layer for the app to save and cache app Data.")),
                         .p(.class("projectDescription"), .b("• Alamofire:"), .text(" for HTTPs request such as POST, GET, PATCH, DELETE. and also for downloading files into the user’s device.")),
                         .p(.class("projectDescription"), .b("• WebKit:"),.text(" for integrating and displaying established web pages.")),
                         .p(.class("projectDescription"), .b("• Google Firebase:"), .text(" for Notification services integration."))
                        
                        
                    ),//div
                    .p(.text("&ensp;")), // for space
                    .div(.class("divPortfolio"),
                         .p(.class("projectDescription"), .b("• KingFisher:"), .text(" for caching images.")),
                         .p(.class("projectDescription"), .b("• CoreAnimation:"), .text(" for animations.")),
                         .p(.class("projectDescription"), .b("• MVC:"), .text(" as the main architecture of the application.")),
                         .p(.class("projectDescription"), .b("• Grand Central Dispatch(GCD):"), .text(" for Multithreading.")),
                         .p(.class("projectDescription"), .b("• UIKit:"),.text(" Developing a responsive UI for all iPhones using interface builder(StoryBoard) and programmatic UI.")),
                         .a(.href("https://apps.apple.com/US/app/id1499315613"),.img(.class("installAppStore"),.src("images/downloadOnAppStore.png")))
                    ),//div
                    .div(.class("click-zoom"),
                         // .label(
                        //    .input(.type(.checkbox)),
                        .img( .src("images/spareTwo.png"))
                        // )//label
                    ),
                    
                    //MARK: - Second Bounou PORTFOLIO PROJECT
                    .p(.text("&ensp;")), // for space
                    .h2(.class("inlineTitle"), .text("‎‎‏‏‎ ‎")),
                    .p(.text("")),
                    .img(.class("portfolioAppIcon"),.src("images/bounouLogo.png")),
                    .p(.text("")),
                    .h1(.class("portfolioAppTitle"), .text("  Bounou")),
                    .a(.href(URL(string: "#")!), .class("rightElementSeeAll inlineTitle"), .text("See More")),
                    .p(.text("")),
                                       
                    .div(.class(" "),
                         .p(.class("projectDescription"),.text(" A fin-tech app that allows users to connect to pay in stores with QR Code, top-Up their accounts, track and analyze spendings, biometrics authentication including FaceID and Finger Print, and set up a business accounts to accept payments as well, etc.")),
                         .p(.class("projectDescription"),.text("Worked closely with product owner and designer to define rich experience, and built the app as a solo iOS Developer and defined tasks also for android as the lead mobile."))
        
                        
                    ),//div
                    .div(.class("click-zoom-4-screens"),
                         //.label(
                        //      .input(.type(.checkbox)),
                        .img(.src("images/bounou4screens.png"))
                        //)//label
                    ),
                    
                        .div(.class(" "),
                        .img(.class("technologiesUsed"),.src("images/bounouTechnologies.png")),
                        
                    
                        .div(.class("installAppStore"),
                             .a(.href("https://apps.apple.com/tn/app/bounou/id1584108227"),.img(.class("installAppStore"),.src("images/downloadOnAppStore.png")))
                        )
                    ),
                    
                    .p(.text("&ensp;")), // for space

                    .p(.text("&ensp;")), // for space
                    .p(.text("&ensp;")), // for space
                    .p(.text("&ensp;")), // for space
                    .p(.text("&ensp;")), // for space
                    .p(.text("&ensp;")), // for space
                    
                    //MARK: - Second Tadawi PORTFOLIO PROJECT

                    .p(.text("")),
                    .img(.class("portfolioAppIcon"),.src("images/croppedTadaw.gif")),
                    .p(.text("")),
                    .h1(.class("portfolioAppTitle"), .text("  TADAWI")),
                    //.h1(.img(.class("portfolioAppIcon"),.src("images/croppedTadaw.gif")),.class("portfolioAppTitle"), .text("  TADAWI")),
                    .a(.href(URL(string: "#")!), .class("rightElementSeeAll inlineTitle"), .text("See More")),
                    .p(.text("")),
                    
                    //.h2(.class("inlineTitle"), .text("Tadawi")),
                    .div(.class("click-zoom"),
                         //.label(
                        //  .input(.type(.checkbox)),
                        .img(.class("imageApp"),.src("images/tadawiOne.png"))
                        //  )//label
                    ),
                    //.div(.img(.class("portfolioImage"), .src("images/spareOne.jpf"))),
                    .div(.class("divPortfolio"),
                         .p(.class("projectDescription"),.text("E-commerce app for selling medicaments, health care products and so on.")),
                         .p(.class("projectDescription"),.text("supports apple sign-in, Order tracking, Offers , Localization and maps.")),
                         .p(.class("projectDescription"),.text("Have been the main Developer building the app from scratch with numerous technologies and 3rd party libraries such as:")),
                         .p(.b("• CoreData:"),.text(" as the persistent layer for App data")),
                         .p(.class("projectDescription"), .b("• NSCache:"), .text(" for caching images.")),
                         .p(.class("projectDescription"), .b("• Apple Maps:"),.text(" so the user can locate his/her location or changing it for the app.")),
                         .p(.class("projectDescription"), .b("• UIKit:"), .text(" Developing a responsive UI for all iPhones using the interface builder(StoryBoard) and programmatic UI."))
                        
                        
                    ),//div
                    .p(.text("&ensp;")), // for space
                    .div(.class("divPortfolio"),
                         .p(.class("projectDescription"), .b("• AppAuth:"), .text(" for users authentication with an OpenID IdentityProvider; login, logout, Registering.")),
                         .p(.class("projectDescription"),  .b("• CoreAnimation, Hero:"), .text(" for animations.")),
                         .p(.class("projectDescription"), .b("• MVVM:"), .text(" as the main architecture of the application. and Multithreading with Grand Central Dispatch(GCD).")),
                         .p(.class("projectDescription"),.b("• Firebase Crashlytics:"), .text(" used for crash reporting.")),
                         .p(.class("projectDescription"), .b("• URLSession:"),.text(" native networking approach for HTTPs requests such as GET, POST, PATCH, DELETE.")),
                         .a(.href("https://apps.apple.com/KW/app/id1507245503"),.img(.class("installAppStore"),.src("images/downloadOnAppStore.png")))
                        
                    ),//div
                    .div(.class("click-zoom"),
                         //.label(
                        //.input(.type(.checkbox)),
                        .img( .src("images/tadawiTwo.png"))
                        //)//label
                    )
                    //.div(.img(.class("portfolioImage"),.src("images/spareTwo.jpf"))),
                    
                    
                )
                //                .myFooter(for: context)
            )//body
            
        )//HTML
    }
    
    func makeSectionHTML(for section: Section<Site>, context: PublishingContext<Site>) throws -> HTML {
        if section.title == "Articles" {
            return HTML(
                .head(for: section, on: context.site),
                .body(
                    .myHeader(for: context),
                    .wrapper(
                        .ul(
                            .class("item-list"),
                            .forEach(context.allItems(sortedBy: \.date, order: .descending), { item in
                                .li(
                                    .article(
                                        .h1(
                                            .a(
                                                .href(item.path),
                                                .text(item.title)
                                            )//a
                                        ), //h1
                                        .p(.text(item.description))
                                    )
                                )
                            })
                        )
                    )
                )
            )
        } else if section.title == "About" {
            //MARK: - About Me
            return HTML(
                .head(for: section, on: context.site),
                .body(
                    .myHeader(for: context),
                    .wrapper(
                        //MARK: - About Me
                        .h2(.class("inlineTitle"), .text("About Me")),
                        .p(.text("")),// for spaces
                        .p(.text("")),// for spaces
                        .p(.text("")),// for spaces
                        .div(.class("socialLinks"),
                             .a(.href("https://twitter.com/HellalMoussa"), .img(.class("scoialIcons"), .src("../images/twitter.svg"))),
                             .a(.href("https://medium.com/@moussahellal"), .img(.class("scoialIcons"), .src("../images/medium.svg"))),
                             .a(.href("https://www.linkedin.com/in/moussa-hellal-834418167"), .img(.class("scoialIcons"), .src("../images/linkidin.svg")))
                        ),//Div social icons links

                                            
                        
                            .p(.class("aboutMeText"), .text("Hello, my name is Moussa, and I have over 5 years of experience as a software developer. I began my career as a Backend developer, where I gained valuable expertise in building scalable and efficient systems. After a year or so in Backend development, I discovered my passion for mobile development and decided to transition to iOS Development. Over the past 4 years, I have honed my skills in developing iOS applications and focused on creating high-quality, user-friendly, and engaging applications that meet the ever-changing needs of mobile users." )),
                        
                        
                        
                        .p(.text("&ensp;")), // for space
                        .p(.text("Generated using "), .a(.class("publishAbout"),.text("Publish"),.target(.blank),.href("https://github.com/JohnSundell/Publish")))
                        
                    )//wrapper
                )//body
            )
        }
        return HTML(
            .head(for: section, on: context.site),
            .body(
                .myHeader(for: context),
                .wrapper(
                    
                    //MARK: - CW PORTFOLIO PROJECT
                    .p(.text("&ensp;")), // for space
                    .h2(.class("inlineTitle"), .text("‎‎‏‏‎ ‎")),
                    .h1(.class("portfolioAppTitle"), .text("  Care.Labs Wallet")),
                    .p(.text("")),
                    .img(.class("portfolioAppIcon"),.src("../images/care.labs_logo.png")),
                    .p(.text("")),
                    
                        .div(.class(" "),
                             .p(.class("projectDescription"),.text(" Mainly for businesses that want to build and leverage their solutions on top of Blockchain technology.")),
                             .p(.class("projectDescription"),.text("Care.Labs Wallet is a breakthrough in Web 3.0 development. It is the app for digital decentralized health network (Care.Network) authors. Using this app, it can easily create a complete commercial and market ready applications also known as Care.Networks.")),
                            
                             .p(.class("projectDescription"),.text("My duty was mainly around what's next, provide solutions suggestion to architects, communicating with products managers, designers, as well as be part of cross team collaboration, and lead the iOS dev team while also being involved in building the apps."))
                        ),
                    
                        .div(.class("click-zoom-4-screens"),
                             //.label(
                            //      .input(.type(.checkbox)),
                            .img(.src("../images/carelabswalletScreenshots.png"))
                            //)//label
                        ),

                        .img(.class("technologiesUsed"),.src("../images/cwlcwltech.png")),
                                            
                        .div(.class("installAppStore"),
                             .a(.href("https://apps.apple.com/us/app/care-labs-wallet/id1621537897"),.img(.class("installAppStore"),.src("../images/downloadOnAppStore.png")))
                        ),
                   
                    .p(.text("")),
                    .img(.class("portfolioAppIcon"),.src("../images/care.wallet_logo.png")),
                    .p(.text("")),
                    .h1(.class("portfolioAppTitle"), .text("  Care.Wallet")),
                    .a(.href(URL(string: "#")!), .class("rightElementSeeAll inlineTitle"), .text("See More")),
                    .p(.text("")),
                                       
                    .div(.class(" "),
                         .p(.class("projectDescription"),.text("Mainly for consumers who want to register their history records, interact with other members within the blockchain, and collect their ECG data through devices such as AliveCor.")),
                         
                         .p(.class("projectDescription"),.text("Care.Wallet uses blockchain technology and puts the user in complete control of who has access to their data, top-up wallet SOLVE balance. Furthermore, a huge part of the app is based on declared and dynamic views into iOS as Server-driven-UI.")),
                        
                         .p(.class("projectDescription"),.text("My duty was mainly around what's next, provide solutions suggestion to architects, communicating with products managers, designers, as well as be part of cross team collaboration, and lead the iOS dev team while also being involved in building the apps."))
        
                        
                    ),//div
                    .div(.class("click-zoom-4-screens"),
                         //.label(
                        //      .input(.type(.checkbox)),
                        .img(.src("../images/cwScreenshots.png"))
                        //)//label
                    ),
                    
                        .div(.class(" "),
                        .img(.class("technologiesUsed"),.src("../images/cwlcwltech.png")),
                        
                    
                        .div(.class("installAppStore"),
                             .a(.href("https://apps.apple.com/us/app/care-wallet/id1434240426"),.img(.class("installAppStore"),.src("../images/downloadOnAppStore.png")))
                        )
                    ),
                    
                    .p(.text("&ensp;")), // for space
                    
                    
                    
                    
                    //MARK: - Second Swapfiets PORTFOLIO PROJECT
                    .p(.text("&ensp;")), // for space
                    .h2(.class("inlineTitle"), .text("‎‎‏‏‎ ‎")),
                    .p(.text("")),
                    .img(.class("portfolioAppIcon"),.src("../images/swapfiets_logo.png")),
                    .p(.text("")),
                    .h1(.class("portfolioAppTitle"), .text("  Swapfiets")),
                    .a(.href(URL(string: "#")!), .class("rightElementSeeAll inlineTitle"), .text("See More")),
                    .p(.text("")),
                                       
                    .div(.class(" "),
                         .p(.class("projectDescription"),.text("Worked with a Dutch agency LabelA as a Contractor for Swapfiets the app to help deliver a new version (3.3.0) and fix few bugs.")),
                         .p(.class("projectDescription"),.text("My main mission was to add new features to the iOS application; such as collecting user feedback within the app with Usabilla Framework. As well as updates to UI and bug fixes."))
        
                        
                    ),//div
                    .div(.class("click-zoom-4-screens"),
                         //.label(
                        //      .input(.type(.checkbox)),
                        .img(.src("../images/swapfiets4screens.png"))
                        //)//label
                    ),
                    
                        .div(.class(" "),
                        .img(.class("technologiesUsed"),.src("../images/cwlcwltech.png")),
                        
                    
                        .div(.class("installAppStore"),
                             .a(.href("https://apps.apple.com/us/app/swapfiets/id1330923084"),.img(.class("installAppStore"),.src("../images/downloadOnAppStore.png")))
                        )
                    ),
                    
                    .p(.text("&ensp;")), // for space
                    
                    
                    //MARK: - Second SPARE PORTFOLIO PROJECT
                    .p(.text("&ensp;")), // for space
                    .h2(.class("inlineTitle"), .text("‎‎‏‏‎ ‎")),
                    .p(.text("")),
                    .img(.class("portfolioAppIcon"),.src("../images/SpareCropped.png")),
                    .p(.text("")),
                    .h1(.class("portfolioAppTitle"), .text("  SPARE")),
                    .a(.href(URL(string: "#")!), .class("rightElementSeeAll inlineTitle"), .text("See More")),
                    .p(.text("")),
                    
                    //.h2(.class("inlineTitle"), .text("Tadawi")),
                    .div(.class("click-zoom"),
                         //.label(
                        //      .input(.type(.checkbox)),
                        .img(.src("../images/spareOne.png"))
                        //)//label
                    ),
                    .div(.class("divPortfolio"),
                         .p(.class("projectDescription"),.text(" A fin-tech app that allows users to track and analyze spendings. Set budget and so on and so forth.")),
                         .p(.class("projectDescription"),.text(" Being in a team while building Spare app with multiple technologies like:")),
                         .p(.b("• Realm:"),.text("a persistent layer for the app to save and cache app Data.")),
                         .p(.class("projectDescription"), .b("• Alamofire:"), .text(" for HTTPs request such as POST, GET, PATCH, DELETE. and also for downloading files into the user’s device.")),
                         .p(.class("projectDescription"), .b("• WebKit:"),.text(" for integrating and displaying established web pages.")),
                         .p(.class("projectDescription"), .b("• Google Firebase:"), .text(" for Notification services integration."))
                        
                        
                    ),//div
                    .p(.text("&ensp;")), // for space
                    .div(.class("divPortfolio"),
                         .p(.class("projectDescription"), .b("• KingFisher:"), .text(" for caching images.")),
                         .p(.class("projectDescription"), .b("• CoreAnimation:"), .text(" for animations.")),
                         .p(.class("projectDescription"), .b("• MVC:"), .text(" as the main architecture of the application.")),
                         .p(.class("projectDescription"), .b("• Grand Central Dispatch(GCD):"), .text(" for Multithreading.")),
                         .p(.class("projectDescription"), .b("• UIKit:"),.text(" Developing a responsive UI for all iPhones using interface builder(StoryBoard) and programmatic UI.")),
                         .a(.href("https://apps.apple.com/US/app/id1499315613"),.img(.class("installAppStore"),.src("../images/downloadOnAppStore.png")))
                    ),//div
                    .div(.class("click-zoom"),
                         // .label(
                        //    .input(.type(.checkbox)),
                        .img( .src("../images/spareTwo.png"))
                        // )//label
                    ),
                    
                    //MARK: - Second Bounou PORTFOLIO PROJECT
                    .p(.text("&ensp;")), // for space
                    .h2(.class("inlineTitle"), .text("‎‎‏‏‎ ‎")),
                    .p(.text("")),
                    .img(.class("portfolioAppIcon"),.src("../images/bounouLogo.png")),
                    .p(.text("")),
                    .h1(.class("portfolioAppTitle"), .text("  Bounou")),
                    .a(.href(URL(string: "#")!), .class("rightElementSeeAll inlineTitle"), .text("See More")),
                    .p(.text("")),
                                       
                    .div(.class(" "),
                         .p(.class("projectDescription"),.text(" A fin-tech app that allows users to connect to pay in stores with QR Code, top-Up their accounts, track and analyze spendings, biometrics authentication including FaceID and Finger Print, and set up a business accounts to accept payments as well, etc.")),
                         .p(.class("projectDescription"),.text("Worked closely with product owner and designer to define rich experience, and built the app as a solo iOS Developer and defined tasks also for android as the lead mobile."))
        
                        
                    ),//div
                    .div(.class("click-zoom-4-screens"),
                         //.label(
                        //      .input(.type(.checkbox)),
                        .img(.src("../images/bounou4screens.png"))
                        //)//label
                    ),
                    
                        .div(.class(" "),
                        .img(.class("technologiesUsed"),.src("../images/bounouTechnologies.png")),
                        
                    
                        .div(.class("installAppStore"),
                             .a(.href("https://apps.apple.com/tn/app/bounou/id1584108227"),.img(.class("installAppStore"),.src("../images/downloadOnAppStore.png")))
                        )
                    ),
                    
                    .p(.text("&ensp;")), // for space

                    .p(.text("&ensp;")), // for space
                    .p(.text("&ensp;")), // for space
                    .p(.text("&ensp;")), // for space
                    .p(.text("&ensp;")), // for space
                    .p(.text("&ensp;")), // for space
                    
                    //MARK: - Second Tadawi PORTFOLIO PROJECT

                    .p(.text("")),
                    .img(.class("portfolioAppIcon"),.src("../images/croppedTadaw.gif")),
                    .p(.text("")),
                    .h1(.class("portfolioAppTitle"), .text("  TADAWI")),
                    //.h1(.img(.class("portfolioAppIcon"),.src("images/croppedTadaw.gif")),.class("portfolioAppTitle"), .text("  TADAWI")),
                    .a(.href(URL(string: "#")!), .class("rightElementSeeAll inlineTitle"), .text("See More")),
                    .p(.text("")),
                    
                    //.h2(.class("inlineTitle"), .text("Tadawi")),
                    .div(.class("click-zoom"),
                         //.label(
                        //  .input(.type(.checkbox)),
                        .img(.class("imageApp"),.src("../images/tadawiOne.png"))
                        //  )//label
                    ),
                    //.div(.img(.class("portfolioImage"), .src("images/spareOne.jpf"))),
                    .div(.class("divPortfolio"),
                         .p(.class("projectDescription"),.text("E-commerce app for selling medicaments, health care products and so on.")),
                         .p(.class("projectDescription"),.text("supports apple sign-in, Order tracking, Offers , Localization and maps.")),
                         .p(.class("projectDescription"),.text("Have been the main Developer building the app from scratch with numerous technologies and 3rd party libraries such as:")),
                         .p(.b("• CoreData:"),.text(" as the persistent layer for App data")),
                         .p(.class("projectDescription"), .b("• NSCache:"), .text(" for caching images.")),
                         .p(.class("projectDescription"), .b("• Apple Maps:"),.text(" so the user can locate his/her location or changing it for the app.")),
                         .p(.class("projectDescription"), .b("• UIKit:"), .text(" Developing a responsive UI for all iPhones using the interface builder(StoryBoard) and programmatic UI."))
                        
                        
                    ),//div
                    .p(.text("&ensp;")), // for space
                    .div(.class("divPortfolio"),
                         .p(.class("projectDescription"), .b("• AppAuth:"), .text(" for users authentication with an OpenID IdentityProvider; login, logout, Registering.")),
                         .p(.class("projectDescription"),  .b("• CoreAnimation, Hero:"), .text(" for animations.")),
                         .p(.class("projectDescription"), .b("• MVVM:"), .text(" as the main architecture of the application. and Multithreading with Grand Central Dispatch(GCD).")),
                         .p(.class("projectDescription"),.b("• Firebase Crashlytics:"), .text(" used for crash reporting.")),
                         .p(.class("projectDescription"), .b("• URLSession:"),.text(" native networking approach for HTTPs requests such as GET, POST, PATCH, DELETE.")),
                         .a(.href("https://apps.apple.com/KW/app/id1507245503"),.img(.class("installAppStore"),.src("../images/downloadOnAppStore.png")))
                        
                    ),//div
                    .div(.class("click-zoom"),
                         //.label(
                        //.input(.type(.checkbox)),
                        .img( .src("../images/tadawiTwo.png"))
                        //)//label
                    )
                    //.div(.img(.class("portfolioImage"),.src("images/spareTwo.jpf"))),
                    
                )
            )
        )//HTML
    }
    
    func makeItemHTML(for item: Item<Site>, context: PublishingContext<Site>) throws -> HTML {
        HTML(
            .head(for: item, on: context.site),
            .body(
                .myHeader(for: context),
                .wrapper(
                    .article(
                        .contentBody(item.body)
                    )
                )
            )
        )//HTML
    }
    
    func makePageHTML(for page: Page, context: PublishingContext<Site>) throws -> HTML {
        try makeIndexHTML(for: context.index, context: context)
    }
    
    func makeTagListHTML(for page: TagListPage, context: PublishingContext<Site>) throws -> HTML? {
        nil
    }
    
    func makeTagDetailsHTML(for page: TagDetailsPage, context: PublishingContext<Site>) throws -> HTML? {
        nil
    }
    
    //
}

extension Theme {
    static var myTheme: Theme {
        Theme(htmlFactory: MyHtmlFactory(), resourcePaths: ["Resources/MyTheme/styles.css"])
    }
}

// This will generate your website using the built-in Foundation theme:
try Moussa().publish(withTheme: .myTheme)


